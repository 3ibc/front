FROM --platform=linux/arm64 node:20-alpine as build-step
WORKDIR /app
COPY . /app
RUN npm i
RUN npm run build

FROM --platform=linux/arm64 nginx:latest
COPY --from=build-step /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80/tcp
CMD ["usr/sbin/nginx", "-g", "daemon off;"]
