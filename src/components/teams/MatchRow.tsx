import {IMatchId} from "../../models/match.model";
import {ITeamId} from "../../models/team.model";

export interface MatchRowAttributes {
    match: IMatchId;
}

function MatchRow(attributes: MatchRowAttributes) {
    return (
        <div>
            <h2>{attributes.match.date.toString()}</h2>
            <table>
                <thead>
                    <tr>
                        <th>Domicile</th>
                        <th>Exterieur</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{(attributes.match.home.team as ITeamId)?.name}</td>
                        <td>{(attributes.match.away.team as ITeamId)?.name}</td>
                    </tr>
                    <tr>
                        <td>{attributes.match.home.score}</td>
                        <td>{attributes.match.away.score}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default MatchRow;