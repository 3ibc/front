import {Navigate, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {IMatchId} from "../../models/match.model";
import {MatchService} from "../../services/match.service";
import {ServiceErrorCode} from "../../services/service.result";
import MatchRow from "./MatchRow";

function TeamMatches() {
    const token = localStorage.getItem("token");
    const params = useParams();
    const teamId = params.team_id;
    const [isLoading, setIsLoading] = useState(true);
    const [matches, setMatches] = useState<IMatchId[]>();

    useEffect(() => {
        const fetchMatches = async () => {
            const res = await MatchService.getAll(token!, teamId);
            if(res.errorCode === ServiceErrorCode.success && res.result) {
                setMatches(res.result);
            }
            setIsLoading(false);
        };

        fetchMatches();
    }, [token, teamId]);

    if(isLoading) {
        return (
            <div>Chargement des matches</div>
        );
    }
    if(matches) {
        return (
            <div>
                {matches.map((m) => {
                    return <MatchRow key={m._id} match={m} />
                })}
            </div>
        )
    }
    return <Navigate to="/login" />
}

export default TeamMatches;