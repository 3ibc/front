import {ITeamId} from "../../models/team.model";
import {useNavigate} from "react-router-dom";

export interface TeamRowAttributes {
    team: ITeamId;
}

function TeamRow(attributes: TeamRowAttributes) {
    const navigate = useNavigate();

    const handleTeamMatches = () => {
        navigate(attributes.team._id);
    };
    return (
        <tr>
            <td>{attributes.team.name}</td>
            <td>{attributes.team.members}</td>
            <td>{attributes.team.city}</td>
            <td>{attributes.team.country}</td>
            <td><button onClick={handleTeamMatches}>Voir</button></td>
        </tr>
    );
}

export default TeamRow;