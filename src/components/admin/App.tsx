import React from 'react';
import './App.css';
import {TeamService} from "../../services/team.service";
import {ServiceErrorCode} from "../../services/service.result";
import {NavLink} from "react-router-dom";

function App() {
    return (
        <div>
            <h1>Admin</h1>
            <NavLink to="../team">Teams</NavLink>
        </div>
    )
}

export default App;
