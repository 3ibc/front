import {useEffect, useState} from "react";
import {ITeamId} from "../../../models/team.model";
import {TeamService} from "../../../services/team.service";
import {ServiceErrorCode} from "../../../services/service.result";
import {Navigate} from "react-router-dom";
import TeamRowAdmin from "./TeamRowAdmin";

function TeamListAdmin() {
    const token = localStorage.getItem("token");
    const [isLoading, setIsLoading] = useState(true);
    const [teams, setTeams] = useState<ITeamId[]>([]);

    useEffect(() => {
        const fetchTeams = async () => {
            const res = await TeamService.getAll(token!);
            if(res.errorCode === ServiceErrorCode.success && res.result) {
                setTeams(res.result);
            }
            setIsLoading(false);
        };
        fetchTeams();
    }, [token]);

    if(isLoading) {
        return (
            <div>Loading teams...</div>
        );
    }
    if(teams) {
        return (
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Members</th>
                    <th>City</th>
                    <th>Country</th>
                </tr>
                </thead>
                <tbody>
                {teams.map(team => (
                    <TeamRowAdmin key={team._id} team={team} />
                ))}
                </tbody>
            </table>
        );
    }

    return (
        <Navigate to="/login" />
    );
}

export default TeamListAdmin;
