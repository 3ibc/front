import {useNavigate, useParams} from "react-router-dom";
import {ChangeEvent, useEffect, useState} from "react";
import {TeamService} from "../../../services/team.service";
import {ITeamId} from "../../../models/team.model";
import {ServiceErrorCode} from "../../../services/service.result";

function TeamUpdateAdmin() {
    const token = localStorage.getItem('token');
    const params = useParams();
    const teamId = params.team_id;
    const [team, setTeam] = useState<ITeamId>();
    const [isLoading, setIsLoading] = useState(true);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchTeam = async () => {
            if(teamId) {
                const sr = await TeamService.getById(teamId, token!);
                if(sr.errorCode === ServiceErrorCode.success && sr.result) {
                    setTeam(sr.result)
                }
            }
            setIsLoading(false);
        }
        fetchTeam();
    }, [token]);

    const handleChangeName = (event: ChangeEvent<HTMLInputElement>) => {
        setTeam((t) => {
            t!.name = event.target.value;
            return t;
        });
    }

    const handleChangeCountry = (event: ChangeEvent<HTMLInputElement>) => {
        setTeam((t) => {
            t!.country = event.target.value;
            return t;
        });
    }

    const handleChangeMembers = (event: ChangeEvent<HTMLInputElement>) => {
        setTeam((t) => {
            t!.members = parseInt(event.target.value);
            return t;
        });
    }

    const handleUpdate = async () => {
        if(team) {
            const sr = await TeamService.updateTeam(team, token!);
            if(sr.errorCode === ServiceErrorCode.success) {
                navigate(-1);
            }
        }
    }

    const handleDelete = async () => {
        if(teamId) {
            const sr = await TeamService.deleteTeam(teamId, token!);
            if(sr.errorCode === ServiceErrorCode.success) {
                navigate(-1);
            }
        }
    }

    if(isLoading) {
        return (
            <p>Loading team...</p>
        )
    }
    if(team) {
        return (
            <div>
                <div>
                    <input type="text" defaultValue={team.name} onChange={handleChangeName}/>
                    <input type="text" defaultValue={team.city} readOnly={true} />
                    <input type="text" defaultValue={team.country} onChange={handleChangeCountry}/>
                    <input type="number" defaultValue={team.members} onChange={handleChangeMembers}/>
                </div>
                <div>
                    <button onClick={handleUpdate}>update</button>
                    <button onClick={handleDelete}>delete</button>
                </div>
            </div>
        )
    }
    return (
        <p>Not matching team</p>
    )
}

export default TeamUpdateAdmin;
