import {useNavigate} from "react-router-dom";
import {ITeamId} from "../../../models/team.model";

export interface TeamRowAdminAttributes {
    team: ITeamId;
}

function TeamRowAdmin(attributes: TeamRowAdminAttributes) {
    const navigate = useNavigate();

    const handleEditTeam = () => {
        navigate(attributes.team._id);
    };
    return (
        <tr>
            <td>{attributes.team.name}</td>
            <td>{attributes.team.members}</td>
            <td>{attributes.team.city}</td>
            <td>{attributes.team.country}</td>
            <td><button onClick={handleEditTeam}>Edit</button></td>
        </tr>
    );
}

export default TeamRowAdmin;