export interface ITeam {
    name: string;
    city: string;
    country: string;
    members: number;
}

export type ITeamId = ITeam & { _id: string };