import {ITeamId} from "./team.model";

export interface ITeamMatch {
    team: string | ITeamId,
    score: number;
}

export interface IMatch {
    home: ITeamMatch;
    away: ITeamMatch;
    date: Date;
}

export type IMatchId = IMatch & {_id: string};