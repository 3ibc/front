import {ITeamId} from "../models/team.model";
import {ServiceResult} from "./service.result";
import axios, {AxiosRequestHeaders} from "axios";
import {APIService} from "./api.service";

export type TeamUpdates = Partial<ITeamId> & Pick<ITeamId, "_id">;

export class TeamService {

    static async getAll(token: string): Promise<ServiceResult<ITeamId[]>> {
        try {
            const res = await axios.get(`${APIService.baseURL}/teams`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                } as AxiosRequestHeaders
            });
            if(res.status === 200) {
                return ServiceResult.success<ITeamId[]>(res.data);
            }
            return ServiceResult.failed();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    static async getById(id:string, token: string): Promise<ServiceResult<ITeamId>> {
        try {
            const res = await axios.get(`${APIService.baseURL}/teams/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                } as AxiosRequestHeaders
            });
            if(res.status === 200) {
                return ServiceResult.success<ITeamId>(res.data);
            }
            return ServiceResult.failed();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    static async deleteTeam(id:string, token: string): Promise<ServiceResult<ITeamId>> {
        try {
            const res = await axios.delete(`${APIService.baseURL}/teams/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                } as AxiosRequestHeaders
            });
            if(res.status === 200) {
                return ServiceResult.success<ITeamId>(res.data);
            }
            return ServiceResult.failed();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

    static async updateTeam(teamUpdates: TeamUpdates, token: string): Promise<ServiceResult<ITeamId>> {
        try {
            const res = await axios.patch(`${APIService.baseURL}/teams/${teamUpdates._id}`, teamUpdates, {
                headers: {
                    'Authorization': `Bearer ${token}`
                } as AxiosRequestHeaders
            });
            if(res.status === 200) {
                return ServiceResult.success<ITeamId>(res.data);
            }
            return ServiceResult.failed();
        } catch(err) {
            return ServiceResult.failed();
        }
    }
}