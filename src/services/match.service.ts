import {ServiceResult} from "./service.result";
import axios, {AxiosRequestHeaders} from "axios";
import {APIService} from "./api.service";
import {IMatchId} from "../models/match.model";

export class MatchService {

    static async getAll(token: string, teamId?: string): Promise<ServiceResult<IMatchId[]>> {
        try {
            let url = `${APIService.baseURL}/matches`;
            if(teamId && teamId.length > 0) {
                url += `?team_id=${teamId}`;
            }
            const res = await axios.get(url, {
                headers: {
                    'Authorization': `Bearer ${token}`
                } as AxiosRequestHeaders
            });
            if(res.status === 200) {
                return ServiceResult.success<IMatchId[]>(res.data);
            }
            return ServiceResult.failed();
        } catch(err) {
            return ServiceResult.failed();
        }
    }

}